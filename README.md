# Overview for Batch Utterance Testing Recipe
This project illustrates three different types of tests:  
* Testing large sets of utterances
* Testing large sets of stream (long-form audio) requests
* Testing large sets of utterances using recorded audio

Each test type is described below, along with shared setup details.

## Common Setup
For test types, do the following:  
* Clone this repo
* Run `npm install` (and make sure Node.js is installed already - [go here if it is not](https://nodejs.org/en/download/))
* Create a Bespoken Virtual Device - [directions here](https://read.bespoken.io/end-to-end/setup)
* Add the Virtual Device to the json files for the tests under the `virtualDevices` section:
  * [recordings-test.json](input/recordings-test.json)
  * [streams-test.json](input/streams-test.json)
  * [utterances-test.json](input/recordings-test.json)
* Make a copy of example.env and save it as `.env` - add the DATADOG_API_KEY here to publish results to DataDog

If you do not have a DataDog API Key, sign up for an account. You can then get the key from:  
`Integrations -> APIs -> API Keys`

## Utterance Tests
Fill out the utterances.csv file with utterances that you want to test.

There are two columns:

| column | description |
| --- | --- |
| utterance | The utterance to send to Alexa and/or Google Assistant
| transcript | The expected transcript result from the assistant

To run the tests locally from the command-line, enter:
```
npm run utterances
```

## Stream Tests
Testing streams is similar, but uses a slightly different set of data for the test.
Fill out the streams.csv file with stream requests that you want to test.

These can be in the form of requesting songs or radio stations.

There are two columns:

| column | description |
| --- | --- |
| utterance | The utterance to send to Alexa and/or Google Assistant
| transcript | The expected transcript result from the assistant
| streamURL | The expected stream URL from the assistant - this corresponds to the media to be played

To run the tests locally from the command-line, enter:
```
npm run streams
```

## Recorded Audio Tests
Testing recordings has the same structure as the utterance tests, but uses URLs instead of regular text for the utterances.

The URLs are automatically resolved as audio and then sent to the assistant.

Supported audio types include:
* mp3
* ogg
* raw (must be 16000 hz, 16-bit, little-endian, single channel)
* wav

There are two columns:

| column | description |
| --- | --- |
| utterance | The URL for the audio recording to send to Alexa and/or Google Assistant
| transcript | The expected transcript result from the assistant

To run the tests locally from the command-line, enter:
```
npm run recordings
```

### Creating Tests From An S3 Bucket
Additionally, there is a version that will automatically create tests from audio files in an S3 bucket.

**NOTE - it requires setting the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in the `.env` file**

Once set, it will use the `sourceBucket` property set in the file [input/recordings-s3-test.json](input/recordings-s3-test.json).

Then run it with:  
```
npm run recordings-s3
```

# Running in Gitlab CI  
## Setup
Add the environment variables need for execution to `Settings -> CI / CD -> Variables`

The variables include:

| Variable | Description
| --- | --- |
| AWS_ACCESS_KEY_ID | The AWS key for the S3 bucket. Only needed for the recordings-s3 test.
| AWS_SECRET_ACCESS_KEY | The AWS key for the S3 bucket. Only needed for the recordings-s3 test.
| DATADOG_API_KEY | The DataDog API key. Needed to publish results to DataDog.

## Adhoc Runs
To run the tests inside Gitlab, do the following:  
* Go to `CI / CD -> Pipelines`
* Select `Run Pipeline`
* Set the variable `JOB_TYPE` to one of `recordings`, `recordings-s3`, `streams` or `utterances`
* Click `Run Pipeline`

That's it! The appropriate job will then run.

## Scheduled Runs
Go to `CI / CD -> Schedules` to set a routine schedule for running the tests.

Be sure to set the JOB_TYPE variable with the appropriate value.

# Viewing Results
## CSV Results
The CSV results are captured with each run.

You can view them in Gitlab by going to:  
`CI / CD -> Jobs`

Then click on the job that corresponds to your run. The test results are under:  
`Job Artifacts -> Browse`

An example is here:  
https://gitlab.com/bespoken/utterance-testing-recipe/-/jobs/424835362

## DataDog Results
DataDog results are published for each test.

View an example here:  
https://p.datadoghq.com/sb/if45szpz39qpe7if-a2f44c7b50b34c6ef3f185fdf7d77b21

Screenshot of the results, for illustration:  

[<img src="docs/DataDogDashboard.png" width="50%">](docs/DataDogDashboard.png)

More info on configuring DataDog dashboards and monitoring is available here:  
https://gitlab.com/bespoken/batch-tester/-/blob/master/docs/datadog.md

