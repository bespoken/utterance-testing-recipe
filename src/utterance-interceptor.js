const { Interceptor, Record, Result } = require('bespoken-batch-tester')

class LongFormInterceptor extends Interceptor {
  /**
   * 
   * @param {Record} record 
   * @param {Result} result 
   */
  interceptResult(record, result) {
    const actualTranscript = result.lastResponse.transcript
    result.success = true
    result.addOutputField('transcript', actualTranscript)
    result.addTag('transcript', actualTranscript)
  }
}

module.exports = LongFormInterceptor